/*
 * $File: syscall.hh
 * $Date: Tue Jun 04 18:08:39 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "ptrace.hh"

#include <list>
#include <memory>

const char* get_syscall_name(int callnum);

class StrInjector {
	struct saved_data_t {
		std::shared_ptr<char> data;
		size_t len, addr;
	};
	size_t m_addr = 0;
	std::list<saved_data_t> m_saved_data;

	public:
		void change(SyscallHandler &hdl, int arg_idx,
				const char *str_new);

		void restore(SyscallHandler &hdl);
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

