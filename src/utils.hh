/*
 * $File: utils.hh
 * $Date: Wed Jun 05 19:46:58 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include <unistd.h>

#include <exception>
#include <string>
#include <functional>

#include <cstdarg>

class FinjError: public std::exception {
	std::string m_msg;

	public:
		FinjError(const char *fmt, ...)
			__attribute__((format(printf, 2, 3)));

		~FinjError() throw () {
		}

		const char *what() const throw () {
			return m_msg.c_str();
		}
};

class LostTrackingError {
	pid_t m_pid;
	public:
		LostTrackingError(int pid):
			m_pid(pid)
		{}

		pid_t pid() const {
			return m_pid;
		}
};

std::string sprintf_cppstr(const char *fmt, ...)
	__attribute__((format(printf, 1, 2)));

std::string vsprintf_cppstr(const char *fmt, va_list ap);

#define log_printf(fmt, ...) \
	__log_printf__(__FILE__, __func__, __LINE__, fmt, ## __VA_ARGS__)

#define ON_EXIT(func) _OnExit _on_exit_name(__LINE__)(func)


void __log_printf__(const char *file, const char *func, int line,
		const char *fmt, ...) __attribute__((format(printf, 4, 5)));

class _OnExit {
	std::function<void()> m_func;

	public:
		_OnExit(const std::function<void()> &func):
			m_func(func)
		{}

		_OnExit() { m_func(); }
};
#define __on_exit_name(x) __on_exit__ ## x 
#define _on_exit_name(x) __on_exit_name(x)

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

