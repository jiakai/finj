/*
 * $File: syscall.cc
 * $Date: Thu Jun 06 00:53:17 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "syscall.hh"
#include "utils.hh"

#include <exception>

#include <sys/syscall.h>

#include <cstring>

extern const char* _SYSCALL32[];
extern const int _SYSCALL32_NR;
extern const char* _SYSCALL64[];
extern const int _SYSCALL64_NR;

const char* get_syscall_name(int callnum) {
#ifdef __x86_64__
	if (callnum >= _SYSCALL64_NR)
		return "<unknown>";
	return _SYSCALL64[callnum];
#else
	if (callnum >= _SYSCALL32_NR)
		return "<unknown>";
	return _SYSCALL32[callnum];
#endif
}

void StrInjector::change(SyscallHandler &hdl, int arg_idx,
		const char *str_new) {
	if (!m_addr)
		m_addr = hdl.find_writable_addr();

	size_t len = strlen(str_new) + 1;
	m_saved_data.push_back(saved_data_t());
	auto &cur = m_saved_data.back();
	cur.data.reset(new char[len], [](char*ptr){delete []ptr;});
	cur.len = len;
	cur.addr = m_addr;
	m_addr += len;

	hdl.peek_data(cur.addr, cur.data.get(), len);
	hdl.poke_data(cur.addr, str_new, len);
	hdl.set_arg(arg_idx, cur.addr);
}

void StrInjector::restore(SyscallHandler &hdl) {
	try {
		for (auto &s: m_saved_data)
			hdl.poke_data(s.addr, s.data.get(), s.len);
	} catch (std::exception &e) {
		if (hdl.get_call_num() == __NR_execve)
			log_printf("ignore exception for sys_execve proc %d: %s",
					int(hdl.get_pid()), e.what());
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

