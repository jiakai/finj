asmlinkage long sys_quotactl(unsigned int cmd, const char __user *special,
asmlinkage long sys_inotify_add_watch(int fd, const char __user *path,
asmlinkage long sys_mknodat(int dfd, const char __user * filename, umode_t mode,
asmlinkage long sys_mkdirat(int dfd, const char __user * pathname, umode_t mode);
asmlinkage long sys_unlinkat(int dfd, const char __user * pathname, int flag);
asmlinkage long sys_symlinkat(const char __user * oldname,
			      int newdfd, const char __user * newname);
asmlinkage long sys_linkat(int olddfd, const char __user *oldname,
			   int newdfd, const char __user *newname, int flags);
asmlinkage long sys_renameat(int olddfd, const char __user * oldname,
			     int newdfd, const char __user * newname);
asmlinkage long sys_futimesat(int dfd, const char __user *filename,
asmlinkage long sys_faccessat(int dfd, const char __user *filename, int mode);
asmlinkage long sys_fchmodat(int dfd, const char __user * filename,
asmlinkage long sys_fchownat(int dfd, const char __user *filename, uid_t user,
asmlinkage long sys_openat(int dfd, const char __user *filename, int flags,
asmlinkage long sys_newfstatat(int dfd, const char __user *filename,
asmlinkage long sys_fstatat64(int dfd, const char __user *filename,
asmlinkage long sys_readlinkat(int dfd, const char __user *path, char __user *buf,
asmlinkage long sys_utimensat(int dfd, const char __user *filename,
