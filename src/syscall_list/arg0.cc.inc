#ifdef __NR_acct
set.set(__NR_acct);
#endif
#ifdef __NR_umount
set.set(__NR_umount);
#endif
#ifdef __NR_oldumount
set.set(__NR_oldumount);
#endif
#ifdef __NR_truncate
set.set(__NR_truncate);
#endif
#ifdef __NR_stat
set.set(__NR_stat);
#endif
#ifdef __NR_statfs
set.set(__NR_statfs);
#endif
#ifdef __NR_statfs64
set.set(__NR_statfs64);
#endif
#ifdef __NR_lstat
set.set(__NR_lstat);
#endif
#ifdef __NR_newstat
set.set(__NR_newstat);
#endif
#ifdef __NR_newlstat
set.set(__NR_newlstat);
#endif
#ifdef __NR_stat64
set.set(__NR_stat64);
#endif
#ifdef __NR_lstat64
set.set(__NR_lstat64);
#endif
#ifdef __NR_truncate64
set.set(__NR_truncate64);
#endif
#ifdef __NR_setxattr
set.set(__NR_setxattr);
#endif
#ifdef __NR_lsetxattr
set.set(__NR_lsetxattr);
#endif
#ifdef __NR_getxattr
set.set(__NR_getxattr);
#endif
#ifdef __NR_lgetxattr
set.set(__NR_lgetxattr);
#endif
#ifdef __NR_listxattr
set.set(__NR_listxattr);
#endif
#ifdef __NR_llistxattr
set.set(__NR_llistxattr);
#endif
#ifdef __NR_removexattr
set.set(__NR_removexattr);
#endif
#ifdef __NR_lremovexattr
set.set(__NR_lremovexattr);
#endif
#ifdef __NR_chroot
set.set(__NR_chroot);
#endif
#ifdef __NR_mknod
set.set(__NR_mknod);
#endif
#ifdef __NR_unlink
set.set(__NR_unlink);
#endif
#ifdef __NR_chmod
set.set(__NR_chmod);
#endif
#ifdef __NR_readlink
set.set(__NR_readlink);
#endif
#ifdef __NR_creat
set.set(__NR_creat);
#endif
#ifdef __NR_open
set.set(__NR_open);
#endif
#ifdef __NR_access
set.set(__NR_access);
#endif
#ifdef __NR_chown
set.set(__NR_chown);
#endif
#ifdef __NR_lchown
set.set(__NR_lchown);
#endif
#ifdef __NR_chown16
set.set(__NR_chown16);
#endif
#ifdef __NR_lchown16
set.set(__NR_lchown16);
#endif
#ifdef __NR_utime
set.set(__NR_utime);
#endif
#ifdef __NR_utimes
set.set(__NR_utimes);
#endif
#ifdef __NR_mkdir
set.set(__NR_mkdir);
#endif
#ifdef __NR_chdir
set.set(__NR_chdir);
#endif
#ifdef __NR_rmdir
set.set(__NR_rmdir);
#endif
#ifdef __NR_swapon
set.set(__NR_swapon);
#endif
#ifdef __NR_swapoff
set.set(__NR_swapoff);
#endif
#ifdef __NR_uselib
set.set(__NR_uselib);
#endif
#ifdef __NR_execve
set.set(__NR_execve);
#endif