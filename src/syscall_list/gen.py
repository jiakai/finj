#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: gen.py
# $Date: Wed Jun 05 19:08:10 2013 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import re

call_name_re = re.compile('^asmlinkage.*sys_([0-9a-zA-Z_]*)')
def get_prog(fin):
    rst = list()
    for i in fin.readlines():
        m = call_name_re.match(i)
        if m:
            rst.append(m.group(1))
    return '\n'.join(map(lambda x:
        '#ifdef __NR_{0}\nset.set(__NR_{0});\n#endif'.format(x), rst))


if __name__ == '__main__':
    for i in '2arg', 'arg0', 'arg1':
        with open('{}.h'.format(i)) as fin:
            with open('{}.cc.inc'.format(i), 'w') as fout:
                fout.write(get_prog(fin))

