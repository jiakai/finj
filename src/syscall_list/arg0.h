asmlinkage long sys_acct(const char __user *name);
asmlinkage long sys_umount(char __user *name, int flags);
asmlinkage long sys_oldumount(char __user *name);
asmlinkage long sys_truncate(const char __user *path, long length);
asmlinkage long sys_stat(const char __user *filename,
asmlinkage long sys_statfs(const char __user * path,
asmlinkage long sys_statfs64(const char __user *path, size_t sz,
asmlinkage long sys_lstat(const char __user *filename,
asmlinkage long sys_newstat(const char __user *filename,
asmlinkage long sys_newlstat(const char __user *filename,
asmlinkage long sys_stat64(const char __user *filename,
asmlinkage long sys_lstat64(const char __user *filename,
asmlinkage long sys_truncate64(const char __user *path, loff_t length);
asmlinkage long sys_setxattr(const char __user *path,
asmlinkage long sys_lsetxattr(const char __user *path,
asmlinkage long sys_getxattr(const char __user *path,
asmlinkage long sys_lgetxattr(const char __user *path,
asmlinkage long sys_listxattr(const char __user *path, 
asmlinkage long sys_llistxattr(const char __user *path,
asmlinkage long sys_removexattr(const char __user *path,
asmlinkage long sys_lremovexattr(const char __user *path,
asmlinkage long sys_chroot(const char __user *filename);
asmlinkage long sys_mknod(const char __user *filename, umode_t mode,
asmlinkage long sys_unlink(const char __user *pathname);
asmlinkage long sys_chmod(const char __user *filename, umode_t mode);
asmlinkage long sys_readlink(const char __user *path,
asmlinkage long sys_creat(const char __user *pathname, umode_t mode);
asmlinkage long sys_open(const char __user *filename,
asmlinkage long sys_access(const char __user *filename, int mode);
asmlinkage long sys_chown(const char __user *filename,
asmlinkage long sys_lchown(const char __user *filename,
asmlinkage long sys_chown16(const char __user *filename,
asmlinkage long sys_lchown16(const char __user *filename,
asmlinkage long sys_utime(char __user *filename,
asmlinkage long sys_utimes(char __user *filename,
asmlinkage long sys_mkdir(const char __user *pathname, umode_t mode);
asmlinkage long sys_chdir(const char __user *filename);
asmlinkage long sys_rmdir(const char __user *pathname);
asmlinkage long sys_swapon(const char __user *specialfile, int swap_flags);
asmlinkage long sys_swapoff(const char __user *specialfile);
asmlinkage long sys_uselib(const char __user *library);
asmlinkage long sys_execve(const char __user *filename,
