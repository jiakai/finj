asmlinkage long sys_mount(char __user *dev_name, char __user *dir_name,
asmlinkage long sys_rename(const char __user *oldname, const char __user *newname);
asmlinkage long sys_link(const char __user *oldname, const char __user *newname);
asmlinkage long sys_symlink(const char __user *old, const char __user *new);

