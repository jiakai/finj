#ifdef __NR_quotactl
set.set(__NR_quotactl);
#endif
#ifdef __NR_inotify_add_watch
set.set(__NR_inotify_add_watch);
#endif
#ifdef __NR_mknodat
set.set(__NR_mknodat);
#endif
#ifdef __NR_mkdirat
set.set(__NR_mkdirat);
#endif
#ifdef __NR_unlinkat
set.set(__NR_unlinkat);
#endif
#ifdef __NR_symlinkat
set.set(__NR_symlinkat);
#endif
#ifdef __NR_linkat
set.set(__NR_linkat);
#endif
#ifdef __NR_renameat
set.set(__NR_renameat);
#endif
#ifdef __NR_futimesat
set.set(__NR_futimesat);
#endif
#ifdef __NR_faccessat
set.set(__NR_faccessat);
#endif
#ifdef __NR_fchmodat
set.set(__NR_fchmodat);
#endif
#ifdef __NR_fchownat
set.set(__NR_fchownat);
#endif
#ifdef __NR_openat
set.set(__NR_openat);
#endif
#ifdef __NR_newfstatat
set.set(__NR_newfstatat);
#endif
#ifdef __NR_fstatat64
set.set(__NR_fstatat64);
#endif
#ifdef __NR_readlinkat
set.set(__NR_readlinkat);
#endif
#ifdef __NR_utimensat
set.set(__NR_utimensat);
#endif