/*
 * $File: ptrace.hh
 * $Date: Wed Jun 05 20:20:02 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "bitset.hh"

#include <sys/types.h>
#include <sys/user.h>
#include <functional>
#include <map>
#include <memory>

class SyscallHandler;

/*!
 * attach to a thread and ptrace
 */
class PTracer {
	class Impl;
	std::map<pid_t, std::shared_ptr<Impl>> m_subproc;
	friend class SyscallHandler;

	public:
		PTracer(pid_t pid);

		/*!
		 * \brief wait for entrance or exit from a syscall of any subprocess
		 * \param sensitive_list only wait for syscalls in this list
		 * \return the sycall handler, or nullptr if all children terminated
		 */
		std::shared_ptr<SyscallHandler> wait_syscall(
				const Bitset *sensitive_list = nullptr);
};

class SyscallHandler {
	enum class Type {
		ENTRANCE, EXIT, TRACEE_TERMINATED
	};
	Type m_type;

	std::shared_ptr<PTracer::Impl> m_impl;
	bool m_continued = false, m_can_cont;
	user_regs_struct m_regs;
#ifdef __x86_64__
	decltype(m_regs.rdi) *m_reg_args[6];
#else
	decltype(m_regs.ebx) *m_reg_args[6];
#endif

	friend class PTracer;

	SyscallHandler(Type type,
			const std::shared_ptr<PTracer::Impl> &impl,
			const user_regs_struct *regs = nullptr);

	SyscallHandler(const SyscallHandler&) = delete;
	void operator = (const SyscallHandler&) = delete;

	public:
		~SyscallHandler();

		bool is_entrance() const {
			return m_type == Type::ENTRANCE;
		}

		bool is_exit() const {
			return m_type == Type::EXIT;
		}

		bool is_tracee_term() const {
			return m_type == Type::TRACEE_TERMINATED;
		}

		int get_call_num() const;
		pid_t get_pid() const;
		const char* get_cwd() const;

		size_t get_arg(int idx) const;
		void set_arg(int idx, size_t val);

		void cont();

		size_t find_writable_addr();

		void peek_data(size_t addr, void *buf, size_t len);
		void peek_cstr(size_t addr, char *buf, size_t maxlen);

		void poke_data(size_t addr, const void *buf, size_t len);
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

