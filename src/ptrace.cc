/*
 * $File: ptrace.cc
 * $Date: Thu Jun 06 00:54:48 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "ptrace.hh"
#include "utils.hh"

#include <cctype>
#include <cstring>
#include <cassert>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/syscall.h>
#include <linux/limits.h>

#include <unistd.h>

typedef long word_t;

#define PTRACE_AND_CHECK(req, pid, addr, data) \
	do { \
		if (ptrace(req, pid, addr, data)) { \
			if (errno == ESRCH) { \
				errno = 0; \
				throw LostTrackingError(pid); \
			} \
			throw FinjError("failed to " #req " of proc %d: %m", \
					int(pid)); \
		} \
	} while(0)


class PTracer::Impl {
	pid_t m_pid;
	bool m_syscall_entrance = true,
		 m_opt_set = false,
		 m_first_stop = true, m_first_trap = true;
	size_t m_writable_addr = 0;

	friend class SyscallHandler;
	friend class PTracer;

	public:
		Impl(pid_t pid, bool attach = true);
		inline void set_ptrace_opt();

		size_t find_writable_addr();

		void peek_data(size_t addr, void *buf, size_t len);
		void peek_cstr(size_t addr, char *buf, size_t maxlen);

		void poke_data(size_t addr, const void *buf, size_t len);

		const char* get_cwd();

		void reinit_on_exec() {
			m_writable_addr = 0;
		}
};

PTracer::PTracer(pid_t pid) {
	m_subproc[pid].reset(new Impl(pid));
}

std::shared_ptr<SyscallHandler> PTracer::wait_syscall(
		const Bitset *sensitive_list) {
#define RETURN(type, args...) \
	return std::shared_ptr<SyscallHandler>(new \
			SyscallHandler(SyscallHandler::Type::type, args))

	for (; ;) {
		int status;	
		pid_t tracee_pid = waitpid(-1, &status, __WALL);
		if (tracee_pid == -1) {
			if (errno == ECHILD)
				return nullptr;
			throw FinjError("failed to wait: %m");
		}
		auto tracee_iter = m_subproc.find(tracee_pid);
		if (tracee_iter == m_subproc.end()) {
			// upon new thread, the wait will return child pid before returning
			// parent pid and PTRACE_EVENT_CLONE
			m_subproc[tracee_pid].reset(new Impl(tracee_pid, false));
			tracee_iter = m_subproc.find(tracee_pid);
		}
		auto tracee = tracee_iter->second;
		tracee->set_ptrace_opt();
		if (WIFEXITED(status) || WIFSIGNALED(status)) {
			if (WIFEXITED(status))
				log_printf("proc %d exited with status %d",
						int(tracee_pid), WEXITSTATUS(status));
			else
				log_printf("proc %d terminated due to signal %d",
						int(tracee_pid), WTERMSIG(status));
			m_subproc.erase(tracee_pid);
			RETURN(TRACEE_TERMINATED, tracee);
		}
		if (!WIFSTOPPED(status))
			throw FinjError("unexpected status of proc %d: %d",
					int(tracee_pid), status);

		// handle fork/vfork/clone
		int sig = WSTOPSIG(status);
		if (((status >> 8) & SIGTRAP) == SIGTRAP) {
			int m = (status >> 8) ^ SIGTRAP;
			if (m == (PTRACE_EVENT_FORK << 8) ||
					m == (PTRACE_EVENT_VFORK << 8) ||
					m == (PTRACE_EVENT_CLONE << 8)) {
				unsigned long pid;
				PTRACE_AND_CHECK(PTRACE_GETEVENTMSG, tracee_pid, nullptr, &pid);
				log_printf("new process: %d", int(pid));
				if (m_subproc.find(pid) == m_subproc.end())
					m_subproc[pid].reset(new Impl(pid, false));
				else
					log_printf("proc %d already exists", int(pid));
				sig = 0;
				goto ptcont;
			}
		}

		if (sig != (SIGTRAP | 0x80)) {
			// not a syscall-stop

			if (sig == SIGSTOP && tracee->m_first_stop) {
				tracee->m_first_stop = false;
				sig = 0;
			}
			if (sig == SIGTRAP && tracee->m_first_trap) {
				tracee->m_first_trap = false;
				sig = 0;
				// XXX: who sent SIGTRAP ?!
			}
			goto ptcont;
		}

		user_regs_struct regs;
		PTRACE_AND_CHECK(PTRACE_GETREGS, tracee_pid, nullptr, &regs);
		int call_nr;
#ifdef __x86_64__
		call_nr = regs.orig_rax;
#else
		call_nr = regs.orig_eax;
#endif

		if (call_nr == __NR_execve)
			tracee->reinit_on_exec();

		if (sensitive_list && !sensitive_list->test(call_nr)) {
			sig = 0;
			goto ptcont;
		}

		tracee->m_syscall_entrance ^= 1;
		if (tracee->m_syscall_entrance)
			RETURN(EXIT, tracee, &regs);
		RETURN(ENTRANCE, tracee, &regs);

ptcont:
		PTRACE_AND_CHECK(PTRACE_SYSCALL, tracee_pid, nullptr, sig);
	}
#undef RETURN
}

SyscallHandler::SyscallHandler(
				Type type, const std::shared_ptr<PTracer::Impl> &impl,
				const user_regs_struct *regs):
			m_type(type), m_impl(impl),
			m_can_cont(type != Type::TRACEE_TERMINATED) {
	
	if (regs) {
		m_regs = *regs;
#ifdef __x86_64__
		m_reg_args[0] = &m_regs.rdi;
		m_reg_args[1] = &m_regs.rsi;
		m_reg_args[2] = &m_regs.rdx;
		m_reg_args[3] = &m_regs.r10;
		m_reg_args[4] = &m_regs.r8;
		m_reg_args[5] = &m_regs.r9;
#else
		m_reg_args[0] = &m_regs.ebx;
		m_reg_args[1] = &m_regs.ecx;
		m_reg_args[2] = &m_regs.edx;
		m_reg_args[3] = &m_regs.esi;
		m_reg_args[4] = &m_regs.edi;
		m_reg_args[5] = &m_regs.ebp;
#endif
	}
}

SyscallHandler::~SyscallHandler() {
	if (m_can_cont && !m_continued)
		cont();
}

size_t SyscallHandler::get_arg(int idx) const {
	assert(idx >= 0 && idx < int(sizeof(m_reg_args) / sizeof(m_reg_args[0])));
	return *m_reg_args[idx];
}

const char* SyscallHandler::get_cwd() const {
	return m_impl->get_cwd();
}

void SyscallHandler::set_arg(int idx, size_t val) {
	assert(idx >= 0 && idx < int(sizeof(m_reg_args) / sizeof(m_reg_args[0])));
	*m_reg_args[idx] = val;
	PTRACE_AND_CHECK(PTRACE_SETREGS, m_impl->m_pid, nullptr, &m_regs);
}


int SyscallHandler::get_call_num() const {
#ifdef __x86_64__
		return m_regs.orig_rax;
#else
		return m_regs.orig_eax;
#endif
}

pid_t SyscallHandler::get_pid() const {
	return m_impl->m_pid;
}

void SyscallHandler::cont() {
	assert(m_can_cont && !m_continued);
	PTRACE_AND_CHECK(PTRACE_SYSCALL, m_impl->m_pid, nullptr, 0);
	m_continued = true;
}

size_t SyscallHandler::find_writable_addr() {
	return m_impl->find_writable_addr();
}

void SyscallHandler::peek_data(size_t addr, void *buf, size_t len) {
	m_impl->peek_data(addr, buf, len);
}

void SyscallHandler::peek_cstr(size_t addr, char *buf, size_t maxlen) {
	m_impl->peek_cstr(addr, buf, maxlen);
}

void SyscallHandler::poke_data(size_t addr, const void *buf, size_t len) {
	m_impl->poke_data(addr, buf, len);
}

PTracer::Impl::Impl(pid_t pid, bool attach):
	m_pid(pid)
{
	if (attach)
		PTRACE_AND_CHECK(PTRACE_ATTACH, pid, nullptr, 0);
}

void PTracer::Impl::set_ptrace_opt() {
	if (m_opt_set)
		return;

	m_opt_set = true;

	int opt = PTRACE_O_TRACESYSGOOD;
	opt |= PTRACE_O_TRACEFORK | PTRACE_O_TRACEVFORK | PTRACE_O_TRACECLONE;

	PTRACE_AND_CHECK(PTRACE_SETOPTIONS, m_pid, nullptr, opt);
}

size_t PTracer::Impl::find_writable_addr() {
	if (m_writable_addr)
		return m_writable_addr;
	FILE *fin = fopen(sprintf_cppstr("/proc/%d/maps", int(m_pid)).c_str(), "r");
	if (!fin)
		throw FinjError("failed to open proc maps: %m");
	ON_EXIT([&fin](){fclose(fin);});
	unsigned long long int start_addr, end_addr, offset, inode;
	char perm[10], dev[10], path[1024];
	for (; ;) {
		int n = fscanf(fin, "%llx-%llx %s %llx %s %lld%[^\n]\n",
				&start_addr, &end_addr, perm, &offset,
				dev, &inode, path);
		if (n <= 0)
			throw FinjError("could not find stack address of proc %d",
					int(m_pid));
		const char *p1 = path;
		while (isspace(*p1))
			p1 ++;
		if (!strcmp(p1, "[stack]"))
			return m_writable_addr = start_addr;
	}
}

const char* PTracer::Impl::get_cwd() {
	static thread_local char path[PATH_MAX + 1];
	auto rst = readlink(sprintf_cppstr("/proc/%d/cwd", int(m_pid)).c_str(),
			path, PATH_MAX);
	if (rst < 0)
		throw FinjError("failed to get cwd of proc %d: %m", int(m_pid));
	path[rst] = 0;
	return path;
}

void PTracer::Impl::peek_data(size_t addr, void *buf_, size_t len) {
	auto buf = static_cast<char*>(buf_);
	errno = 0;
	for (size_t offset = 0; offset < len; offset += sizeof(word_t)) {
		word_t tmp = ptrace(PTRACE_PEEKDATA, m_pid, addr + offset, nullptr);
		if (errno)
			throw FinjError("failed to peek data from proc %d(0x%llx, %d): %m",
					int(m_pid), (long long)addr, int(len));
		if (offset + sizeof(word_t) < len)
			*((word_t*)(buf + offset)) = tmp;
		else {
			auto src = (const char*)&tmp;
			while (offset < len)
				buf[offset ++] = *(src ++);
			return;
		}
	}
}

void PTracer::Impl::peek_cstr(size_t addr, char *buf, size_t maxlen) {
	errno = 0;
	for (size_t offset = 0; offset < maxlen; offset += sizeof(word_t)) {
		word_t tmp = ptrace(PTRACE_PEEKDATA, m_pid, addr + offset, nullptr);
		if (errno)
			throw FinjError("failed to peek data from %d: %m", int(m_pid));
		auto src = (const char*)&tmp;
		for (size_t i = 0; i < sizeof(word_t) && i + offset < maxlen; i ++)
			if (!(buf[offset + i] = src[i]))
				return;
	}
	buf[maxlen - 1] = 0;
}

void PTracer::Impl::poke_data(size_t addr, const void *buf, size_t len) {
	auto src = static_cast<const char *>(buf);
	size_t offset = 0;
	for (; offset < len; offset += sizeof(word_t))
		if (ptrace(PTRACE_POKEDATA, m_pid, addr + offset,
					*(word_t*)(src + offset)))
			throw FinjError("failed to poke data to %d(0x%llx, %d): %m",
					int(m_pid), (long long)addr, int(len));
	if (offset > len) {
		offset -= sizeof(word_t);
		word_t data = ptrace(PTRACE_PEEKDATA, m_pid, addr + offset, nullptr);
		char *ptr = (char*)&data;
		for (size_t i = offset; i < len; i ++)
			ptr[i - offset] = src[i];
		PTRACE_AND_CHECK(PTRACE_POKEDATA, m_pid, addr + offset, data);
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

