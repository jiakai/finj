/*
 * $File: main.cc
 * $Date: Thu Jun 06 09:16:08 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "ptrace.hh"
#include "utils.hh"
#include "syscall.hh"

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <map>

#include <sys/types.h>
#include <sys/syscall.h>
#include <linux/limits.h>
#include <signal.h>
#include <unistd.h>
#include <dlfcn.h>

// return: whether changed to new path
typedef int (*chg_path_t)(pid_t pid, int call_num,
		const char *cwd, const char *src, char *dest, int dest_max_len);
typedef int (*inverse_chg_path_t)(pid_t pid, int call_num,
		const char *cwd, const char *src, char *dest, int dest_max_len);

class PathChanger {
	StrInjector m_injector;	
	size_t m_arg0, m_arg1;
	static bool m_first_exec;	// caused by exec in main

	void inject_arg(SyscallHandler &hdl, int idx);

	public:
		void enter(SyscallHandler &hdl);
		void exit(SyscallHandler &hdl);
};
bool PathChanger::m_first_exec = true;

static void trace(pid_t pid);
static chg_path_t chg_path;
static inverse_chg_path_t inv_chg_path;
static void build_syscall_list();

static Bitset syscall_lst_all,
			  syscall_lst_arg1,	// inject argument 1
			  syscall_lst_2arg;	// inject first two arguments

int main(int argc, char **argv) {
	if (argc <= 2) {
		fprintf(stderr, "usage: %s <path replacement .so> "
				"<target program> [<args> ...]\n", argv[0]);
		return -1;
	}
	build_syscall_list();

	void *hdl = dlopen(argv[1], RTLD_LAZY);
	if (!hdl)
		throw FinjError("failed to load %s: %s", argv[1], dlerror());
	chg_path = (chg_path_t)dlsym(hdl, "chg_path");
	inv_chg_path = (chg_path_t)dlsym(hdl, "inv_chg_path");
	if (!chg_path || !inv_chg_path)
		throw FinjError("failed to resolve symbol: %s", dlerror());

	pid_t pid = fork();
	if (pid == -1)
		throw FinjError("failed to fork: %m");
	if (!pid) {
		kill(getpid(), SIGSTOP); // wait for ptrace
		execv(argv[2], argv + 2);
		fprintf(stderr, "failed to execute %s\n", argv[1]);
		exit(-1);
	}
	trace(pid);
}

void trace(pid_t pid) {
	PTracer pt(pid);
	std::map<pid_t, PathChanger> path_changer;
	for (; ;) {
		try {
			auto hdl = pt.wait_syscall(&syscall_lst_all);
			if (!hdl) {
				log_printf("all subprocesses terminated");
				return;
			}
			if (hdl->is_tracee_term())
				continue;
			if (hdl->is_entrance())
				path_changer[hdl->get_pid()].enter(*hdl);
			else {
				auto p = path_changer.find(hdl->get_pid());
				p->second.exit(*hdl);
				path_changer.erase(p);
			}
		} catch (LostTrackingError &e) {
			log_printf("lost tracking to proc %d", int(e.pid()));
		}
	}
}

void build_syscall_list() {
	{
		auto &set = syscall_lst_all;
		set.set(__NR_getcwd);
#include "syscall_list/arg0.cc.inc"
#include "syscall_list/arg1.cc.inc"
#include "syscall_list/2arg.cc.inc"
	}
	{
		auto &set = syscall_lst_arg1;
#include "syscall_list/arg1.cc.inc"
	}
	{
		auto &set = syscall_lst_2arg;
#include "syscall_list/2arg.cc.inc"
	}
}

void PathChanger::enter(SyscallHandler &hdl) {
	int call_nr = hdl.get_call_num();
	if (call_nr == __NR_execve && m_first_exec) {
		m_first_exec = false;
		return;
	}
	if (syscall_lst_arg1.test(call_nr))
		inject_arg(hdl, 1);
	else if (syscall_lst_2arg.test(call_nr)) {
		inject_arg(hdl, 0);
		inject_arg(hdl, 1);
	} else if (call_nr == __NR_getcwd) {
		m_arg0 = hdl.get_arg(0);
		m_arg1 = hdl.get_arg(1);
	} else
		inject_arg(hdl, 0);
}

void PathChanger::exit(SyscallHandler &hdl) {
	if (hdl.get_call_num() == __NR_getcwd) {
		char pold[m_arg1], pnew[m_arg1];
		hdl.peek_cstr(m_arg0, pold, m_arg1);
		if (inv_chg_path(hdl.get_pid(), __NR_getcwd,
					hdl.get_cwd(), pold, pnew, m_arg1)) {
			hdl.poke_data(m_arg0, pnew, strlen(pnew) + 1);
			log_printf("getcwd: change return value from `%s' to `%s'",
					pold, pnew);
		}
	}  else
		m_injector.restore(hdl);
}

void PathChanger::inject_arg(SyscallHandler &hdl, int idx) {
	char path_orig[PATH_MAX], path_new[PATH_MAX];
	int call_nr = hdl.get_call_num();
	{
		auto addr = hdl.get_arg(idx);
		if (!addr) {
			log_printf("%s: null pointer for arg %d",
					get_syscall_name(call_nr), idx);
			return;
		}
		hdl.peek_cstr(addr, path_orig, sizeof(path_orig));
	}
	if (!chg_path(hdl.get_pid(), call_nr,
				hdl.get_cwd(), path_orig, path_new, sizeof(path_new)))
		return;
	m_injector.change(hdl, idx, path_new);
	log_printf("%s: proc %d, change `%s' to `%s'",
			get_syscall_name(call_nr), int(hdl.get_pid()),
			path_orig, path_new);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

