#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: gen_syscall_name.py
# $Date: Wed Jun 05 18:19:41 2013 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import re
call_name_re = re.compile('^#define __NR_([a-zA-Z_0-9]*) ([0-9]*)$')
def parse_header(fin):
    """:return: name of call names"""
    num2name = dict()
    for line in fin.readlines():
        m = call_name_re.match(line)
        if m:
            num2name[int(m.group(2))] = m.group(1)

    return [num2name.get(i, '<unknown>') for i in
            range(max(num2name.keys()) + 1)]


def convert_file(fout, fin_path, var_name):
    with open(fin_path) as f:
        print >> fout, """extern const char* {0}[];
            extern const int {0}_NR;""".format(var_name)
        lst = parse_header(f)
        print >> fout, 'const char *{}[] = {{'.format(var_name)
        print >> fout, ','.join(['"' + i + '"' for i in lst])
        print >> fout, '};'
        print >> fout, 'const int {}_NR = {};'.format(var_name, len(lst))

if __name__ == '__main__':
    with open('syscall_name.cc', 'w') as fout:
        convert_file(fout, "/usr/include/asm/unistd_32.h", '_SYSCALL32')
        convert_file(fout, "/usr/include/asm/unistd_64.h", '_SYSCALL64')
