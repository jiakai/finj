/*
 * $File: bitset.hh
 * $Date: Mon Jun 03 15:04:36 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include <cstdint>
#include <cstddef>

#include <vector>

/*!
 * \brief fast dynamic bitset
 */
class Bitset {
	std::vector<uint32_t> m_buf;
	size_t m_max_pos = 0;

	public:
		inline void set(size_t pos) {
			if (pos > m_max_pos) {
				m_buf.resize((pos >> 5) + 1);
				m_max_pos = pos;
			}
			m_buf[pos >> 5] |= 1 << (pos & 31);
		}

		inline int test(size_t pos) const {
			if (pos > m_max_pos)
				return false;
			return (m_buf[pos >> 5] >> (pos & 31)) & 1;
		}
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

