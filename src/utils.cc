/*
 * $File: utils.cc
 * $Date: Mon Jun 03 14:55:14 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <libgen.h>
#include <pthread.h>
#include <unistd.h>

using namespace std;

FinjError::FinjError(const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	m_msg = vsprintf_cppstr(fmt, ap);
}

string sprintf_cppstr(const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	return vsprintf_cppstr(fmt, ap);
}

string vsprintf_cppstr(const char *fmt, va_list ap_orig) {
	int size = 100;     /* Guess we need no more than 100 bytes */
	char *p;

	if ((p = (char*)malloc(size)) == nullptr)
		goto err;

	for (; ;) {
		va_list ap;
		va_copy(ap, ap_orig);
		int n = vsnprintf(p, size, fmt, ap);
		va_end(ap);

		if (n < 0)
			goto err;

		if (n < size) {
			string rst(p);
			free(p);
			return rst;
		}

		size = n + 1;

		char *np = (char*)realloc(p, size);
		if (!np) {
			free(p);
			goto err;
		} else 
			p = np;
	}

err:
	fprintf(stderr, "could not allocate memory\n");
	abort();
}

void __log_printf__(const char *file, const char *func, int line,
		const char *fmt, ...) {
	static int enable_log = -1;
	static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
	if (enable_log == -1) {
		enable_log = !getenv("FINJ_NO_LOG");
		fprintf(stderr, "finj: enable_log=%d (set FINJ_NO_LOG to disable)\n",
				enable_log);
	}
	if (!enable_log)
		return;

	static const char *time_fmt = nullptr;
	if (!time_fmt) {
		if (isatty(fileno(stderr)))
			time_fmt = "\033[1;31m[%s %s@%s:%d]\033[0m ";
		else
			time_fmt = "[%s %s@%s:%d] ";
	}
	time_t cur_time;
	time(&cur_time);
	char timestr[64];
	strftime(timestr, sizeof(timestr), "%H:%M:%S",
			localtime(&cur_time));

	pthread_mutex_lock(&lock);
	fprintf(stderr, time_fmt, timestr, func, basename(strdupa(file)), line);

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fputc('\n', stderr);

	pthread_mutex_unlock(&lock);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

