#!/bin/bash -e
# $File: init.sh
# $Date: Thu Jun 06 09:57:04 2013 +0800
# $Author: jiakai <jia.kai66@gmail.com>

for i in dev etc lib proc usr
do
	[ -d $i ] || mkdir $i
	sudo mount --bind /$i $i
done
mkdir tmp

