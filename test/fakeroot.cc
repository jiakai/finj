/*
 * $File: fakeroot.cc
 * $Date: Thu Jun 06 09:51:49 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include <cstdlib>
#include <cstring>
#include <cstddef>

#include <vector>
#include <string>

#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <linux/limits.h>

static const char *fakeroot;
static int fakeroot_len;

/*!
 * \brief get realpath based on specified cwd; do not follow symbol links
 */
static const char* rela_realpath(const char *cwd, const char *name);
static void init_fakeroot() __attribute__((constructor));
static inline bool in_fakeroot(const char *s) {
	return !memcmp(s, fakeroot, fakeroot_len) && s[fakeroot_len] == '/';
}

extern "C" int chg_path(pid_t pid, int call_nr,
		const char *cwd, const char *src,
		char *dest, int dest_max_len) {

	if (!in_fakeroot(cwd))
		cwd = "/";
	else
		cwd += fakeroot_len;
	src = rela_realpath(cwd, src);
	snprintf(dest, dest_max_len, "%s%s", fakeroot, src);

	return 1;
}

extern "C" int inv_chg_path(pid_t pid, int call_nr,
		const char *cwd, const char *src,
		char *dest, int dest_max_len) {
	if (!in_fakeroot(dest)) {
		strcpy(dest, "/");
	} else
		strncpy(dest, src + fakeroot_len, dest_max_len);
	return 1;
}

static void init_fakeroot() {
	char *cwd_ptr = get_current_dir_name();
	static std::string cwd(cwd_ptr);
	free(cwd_ptr);
	cwd.append("/fake-root");
	fakeroot = cwd.c_str();
	fakeroot_len = cwd.length();
}


// modified from glibc realpath implementation
const char* rela_realpath (const char *cwd, const char *name) {
	static thread_local std::vector<char> rpath_str;
	long int path_max;

#ifdef PATH_MAX
	path_max = PATH_MAX;
#else
	path_max = pathconf (name, _PC_PATH_MAX);
	if (path_max <= 0)
		path_max = 1024;
#endif

	rpath_str.resize(path_max);

	char *rpath = rpath_str.data(), *dest;
	const char *start, *end, *rpath_limit;

	rpath_limit = rpath + path_max;

	if (name[0] != '/') {
		strcpy(rpath, cwd);
		dest = (char*)rawmemchr(rpath, '\0');
	}
	else {
		rpath[0] = '/';
		dest = rpath + 1;
	}

	for (start = end = name; *start; start = end)
	{
		/* Skip sequence of multiple path-separators.  */
		while (*start == '/')
			++start;

		/* Find end of path component.  */
		for (end = start; *end && *end != '/'; ++end)
			/* Nothing.  */;

		if (end - start == 0)
			break;
		else if (end - start == 1 && start[0] == '.')
			/* nothing */;
		else if (end - start == 2 && start[0] == '.' && start[1] == '.') {
			/* Back up to previous component, ignore if at root already.  */
			if (dest > rpath + 1)
				while ((--dest)[-1] != '/');
		}
		else {
			if (dest[-1] != '/')
				*dest++ = '/';

			if (dest + (end - start) >= rpath_limit) {

				ptrdiff_t dest_offset = dest - rpath;
				size_t new_size = rpath_limit - rpath;

				if (end - start + 1 > path_max)
					new_size += end - start + 1;
				else
					new_size += path_max;

				rpath_str.resize(new_size);
				rpath = rpath_str.data();
				rpath_limit = rpath + new_size;

				dest = rpath + dest_offset;
			}

			memcpy(dest, start, end - start);
			dest += end - start;
			*dest = '\0';
		}
	}
	if (dest > rpath + 1 && dest[-1] == '/')
		--dest;
	*dest = '\0';

	return rpath_str.data();
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
