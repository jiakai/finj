/*
 * $File: performance.cc
 * $Date: Thu Jun 06 18:58:44 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include <cstdio>

#include <sys/time.h>
#include <cstdio>
#include <unistd.h>

static double get_realtime();

class Timer {
	double m_start;
	const char *m_name;
	int m_repeat;
	public:
		Timer(const char *name, int repeat):
			m_start(get_realtime()),
			m_name(name), m_repeat(repeat)
		{
		}

		~Timer() {
			printf("%s %d: %.4lf\n", m_name, m_repeat, 
					get_realtime() - m_start);
		}
};

void test_fork() {
	const int N = 10000;
	Timer timer("fork", N);
	for (int i = 0; i < N; i ++)
		if (!fork())
			_exit(1);
}

void test_openfile() {
	const int N = 10000;
	Timer timer("fopen", N);
	for (int i = 0; i < N; i ++)
		fclose(fopen("/tmp/perf", "w"));
}

void test_write() {
	const int N = 100000;
	Timer timer("write", N);
	FILE *fout = fopen("/tmp/perf", "w");
	for (int i = 0; i < N; i ++)  {
		fwrite(&i, sizeof(i), 1, fout);
		fflush(fout);
	}
	fclose(fout);
}

double get_realtime() {
	timeval tv;
	gettimeofday(&tv, nullptr);
	return tv.tv_sec + tv.tv_usec * 1e-6;
}

int main() {
	test_write();
	test_openfile();
	test_fork();
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

