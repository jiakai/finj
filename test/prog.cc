#include <cstdio>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>

static void create_file(const char *path) {
	FILE *f = fopen(path, "w");
	if (!f)
		printf("failed to create %s: %m\n", path);
	else
		fclose(f);
}

static void* thread(void* wid) {
	char fname[100];
	sprintf(fname, "/tmp/finj/test-%d-%d",
			int(getpid()), int((size_t)wid));
	create_file(fname);
	sprintf(fname, "test-%d-%d-rela",
			int(getpid()), int((size_t)wid));
	create_file(fname);
	char f1[100];
	sprintf(f1, "/tmp/finj/test-%d-%d-rela-moved",
			int(getpid()), int((size_t)wid));
	rename(fname, f1);
	return NULL;
}

static void proc() {
	pthread_t pt0, pt1;
	pthread_create(&pt0, NULL, thread, (void*)0);
	pthread_create(&pt1, NULL, thread, (void*)1);
	pthread_join(pt0, NULL);
	pthread_join(pt1, NULL);
}

int main() {
	chdir("/tmp/finj");
	printf("cwd=%s\n", get_current_dir_name());
	setsid();
	thread(0);
	if (fork())
		_exit(1);
	else {
		if (fork()) {
			usleep(100000);
			proc();
			wait(NULL);
		}
		usleep(200000);
		proc();
	}
}

