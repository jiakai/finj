# $File: Makefile
# $Date: Thu Jun 06 18:55:51 2013 +0800
# $Author: jiakai <jia.kai66@gmail.com>

BUILD_DIR = build
TARGET = finj

CXX = g++ -std=c++11
ARGS = ./$(TARGET) ./test/fakeroot.so /bin/bash #./test/prog

SRC_EXT = cc
CPPFLAGS = -Isrc
override OPTFLAG ?= -O2 -ggdb

override CXXFLAGS += \
	-Wall -Wextra -Wnon-virtual-dtor -Wno-unused-parameter -Winvalid-pch \
	-Wno-unused-local-typedefs \
	$(CPPFLAGS) $(OPTFLAG)
override LDFLAGS += -ldl

CXXSOURCES = $(shell find src -name "*.$(SRC_EXT)")
OBJS = $(addprefix $(BUILD_DIR)/,$(CXXSOURCES:.$(SRC_EXT)=.o))
DEPFILES = $(OBJS:.o=.d)

all: $(TARGET)

$(BUILD_DIR)/%.o: %.$(SRC_EXT)
	@echo "[cxx] $< ..."
	@$(CXX) -c $< -o $@ $(CXXFLAGS)

$(BUILD_DIR)/%.d: %.$(SRC_EXT)
	@mkdir -pv $(dir $@)
	@echo "[dep] $< ..."
	@$(CXX) $(CPPFLAGS) -MM -MT "$@ $(@:.d=.o)" "$<"  > "$@"

sinclude $(DEPFILES)

$(TARGET): $(OBJS)
	@echo "Linking ..."
	@$(CXX) $(OBJS) -o $@ $(LDFLAGS)

clean:
	rm -rf $(BUILD_DIR) $(TARGET)

run: init
	$(ARGS)

performance: init
	FINJ_NO_LOG=1 ./$(TARGET) test/fakeroot.so test/performance
	./test/performance

gdb: init
	gdb --args $(ARGS)

init: $(TARGET)
	(pgrep prog && killall -KILL prog; exit 0) >/dev/null
	make -C test
	rm -rf fake-root/tmp
	mkdir -p fake-root/tmp/finj

git:
	git add -A
	git commit -a

.PHONY: all clean run gdb git init

# vim: ft=make

